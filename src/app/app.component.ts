import {Component, OnInit} from '@angular/core';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [NgbModalConfig, NgbModal]
})
export class AppComponent implements OnInit {
  title = 'ProjectCurd';
  people: any;
  editTitle: string;
  userId: number;
  titleVar: string;
  modelRef: any;
  index: number;
  baseUrl = 'https://jsonplaceholder.typicode.com/posts'
  constructor(config: NgbModalConfig, private modalService: NgbModal, private http: HttpClient) {}

  ngOnInit() {
    this.fetchFunction();
  }

  open(edit, id, title, index) {
    this.modelRef = this.modalService.open(edit);
    this.editTitle = title;
    this.userId = id;
    this.index = index;
  }

  fetchFunction(){
    this.http.get(this.baseUrl).subscribe((response : any) => {
      this.people = response
    });
  }

  deleteFunction() {
    this.http.delete(`${this.baseUrl}/${this.userId}`).subscribe((res) => {
      this.people.splice(this.people.findIndex(e => e.id === this.userId), 1);
      this.modelRef.close() 
    })
  }

  updateFunction(){
    let body =  JSON.stringify({
      id: this.userId,
      title:  this.editTitle,
    })

    const httpOptions = {
      headers: new HttpHeaders({      
        "Content-type": "application/json; charset=UTF-8"
      })
    };

    this.http.put(`${this.baseUrl}/${this.userId}`,body, httpOptions).subscribe((res)=> {
      this.people[this.index].title = this.editTitle
      this.modelRef.close()
    })
  }

}

